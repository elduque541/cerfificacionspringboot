CREATE TABLE product (
   id BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
   name VARCHAR(50) NOT NULL,
   description VARCHAR(20) NOT NULL,
   basePrice DECIMAL (10,2),
   taxRate DECIMAL (10,2),
   productStatus VARCHAR(20),
   inventoryQuantity INTEGER
);