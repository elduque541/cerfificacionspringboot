package com.ias.certificacion.domain;

import com.google.common.base.Preconditions;
import com.ias.certificacion.exceptions.ProductException;
import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;

@Data
public class Product {

    public static Product of(Long productId, String name, String description, BigDecimal basePrice, BigDecimal taxRate,
                             EstadosProducto productStatus, Integer inventoryQuantity) {
        return new Product(productId, name, description, basePrice, taxRate, productStatus, inventoryQuantity);
    }

    private Product(@NonNull Long productId, @NonNull String name, @NonNull String description, @NonNull BigDecimal basePrice,
                    @NonNull BigDecimal taxRate, @NonNull EstadosProducto productStatus, @NonNull Integer inventoryQuantity) {
        Preconditions.checkArgument(productId > 0, "productId must be greater than 0");
        Preconditions.checkArgument(name.length() > 0, "name it can't be empty");
        Preconditions.checkArgument(name.length() < 101, "name can have a maximum of 100 characters");
        Preconditions.checkArgument(description.length() > 0, "description it can't be empty");
        Preconditions.checkArgument(description.length() < 281, "description can have a maximum of 280 characters");
        Preconditions.checkArgument(basePrice.doubleValue() > 0, "basePrice must be greater than 0");
        Preconditions.checkArgument(taxRate.doubleValue() > 0, "taxRate must be greater than 0");
        Preconditions.checkArgument(taxRate.doubleValue() <= 1, "taxRate must not be greater than 1");
        Preconditions.checkArgument(inventoryQuantity > 0, "inventoryQuantity must be greater than 0");

        this.productId = productId;
        this.name = name;
        this.description = description;
        this.basePrice = basePrice;
        this.taxRate = taxRate;
        this.productStatus = productStatus;
        this.inventoryQuantity = inventoryQuantity;
    }

    public enum EstadosProducto {
        Borrador, Publicado
    }

    private final Long productId;
    private final String name;
    private final String description;

    private final BigDecimal basePrice;
    private final BigDecimal taxRate;

    private final EstadosProducto productStatus;

    private final Integer inventoryQuantity;

}
