package com.ias.certificacion.repositories;

import com.ias.certificacion.domain.Product;
import com.ias.certificacion.operation.ProductOperation;
import com.ias.certificacion.operation.ProductOperationSuccess;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRowMapper implements RowMapper<ProductOperation> {

    @Override
    public ProductOperation mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ProductOperationSuccess.of(Product.of(
                rs.getLong("id"),
                rs.getString("name"),
                rs.getString("description"),
                rs.getBigDecimal("basePrice"),
                rs.getBigDecimal("taxRate"),
                Product.EstadosProducto.valueOf(rs.getString("productStatus")),
                rs.getInt("inventoryQuantity")));
    }
}
