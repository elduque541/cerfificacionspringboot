package com.ias.certificacion.repositories;

import com.ias.certificacion.exceptions.ProductDoesNotExists;
import com.ias.certificacion.domain.ProductOperationRequest;
import com.ias.certificacion.operation.ProductOperation;
import com.ias.certificacion.operation.ProductOperationFailure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductRepositoty {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoty(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public ProductOperation insertOne(ProductOperationRequest productOperationRequest) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                .withTableName("product")
                .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", productOperationRequest.getName());
        parameters.put("description", productOperationRequest.getDescription());
        parameters.put("basePrice", productOperationRequest.getBasePrice());
        parameters.put("taxRate", productOperationRequest.getTaxRate());
        parameters.put("productStatus", productOperationRequest.getProductStatus());
        parameters.put("inventoryQuantity", productOperationRequest.getInventoryQuantity());

        return findById(simpleJdbcInsert.executeAndReturnKey(parameters).longValue());
    }

    public ProductOperation updateOne(Long id, ProductOperationRequest productOperationRequest) {
        final String query = "UPDATE product SET name = '" + productOperationRequest.getName() + "' ," +
                " description = '" + productOperationRequest.getDescription() + "' ," +
                " basePrice = '" + productOperationRequest.getBasePrice() + "' ," +
                " taxRate = '" + productOperationRequest.getTaxRate() + "' ," +
                " productStatus = '" + productOperationRequest.getProductStatus() + "' ," +
                "inventoryQuantity = '" + productOperationRequest.getInventoryQuantity() + "' " +
                " WHERE id = " + id + " ";
        int update = jdbcTemplate.update(query);
//        int update = jdbcTemplate.update(
//                query,
//                productOperationRequest.getName(),
//                productOperationRequest.getDescription(),
//                productOperationRequest.getBasePrice(),
//                productOperationRequest.getTaxRate(),
//                productOperationRequest.getProductStatus(),
//                productOperationRequest.getInventoryQuantity(),
//                id);

        if (update == 0) {
            return ProductOperationFailure.of(new ProductDoesNotExists());
        } else {
            return findById(id);
        }
    }

    public ProductOperation deleteOne(Long id) {
        final String query = "DELETE FROM product WHERE id = ?";
        ProductOperation productoToEliminate = findById(id);

        if (productoToEliminate.isValid()) {
            jdbcTemplate.update(query, id);
            return productoToEliminate;
        } else {
            return ProductOperationFailure.of(new ProductDoesNotExists());
        }
    }

    public ProductOperation findById(Long id) {
        final String query = "SELECT * FROM product WHERE id = ?";
        ProductOperation productOperation = null;
        try {
            productOperation = jdbcTemplate.queryForObject(
                    query,
                    new Object[]{id},
                    new CustomerRowMapper());

        } catch (Exception e) {
            productOperation = ProductOperationFailure.of(new ProductDoesNotExists());
        }

        return productOperation;
    }

    public List<ProductOperation> findByAll() {
        final String query = "SELECT * FROM product";
        return jdbcTemplate.query(
                query,
                new CustomerRowMapper());
    }

}
