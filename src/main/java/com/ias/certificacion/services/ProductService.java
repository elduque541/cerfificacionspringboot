package com.ias.certificacion.services;

import com.ias.certificacion.domain.ProductOperationRequest;
import com.ias.certificacion.operation.ProductOperation;
import com.ias.certificacion.repositories.ProductRepositoty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepositoty productRepositoty;

    @Autowired
    public ProductService(ProductRepositoty productRepositoty) {
        this.productRepositoty = productRepositoty;
    }

    public ProductOperation insertOne(ProductOperationRequest productOperationRequest) {
        return productRepositoty.insertOne(productOperationRequest);
    }

    public ProductOperation updateOne(Long id, ProductOperationRequest productOperationRequest) {
        return productRepositoty.updateOne(id, productOperationRequest);
    }

    public ProductOperation deleteOne(Long id) {
        return productRepositoty.deleteOne(id);
    }

    public ProductOperation findById(Long id) {
        return productRepositoty.findById(id);
    }

    public List<ProductOperation> findAll() {
        return productRepositoty.findByAll();
    }
}
