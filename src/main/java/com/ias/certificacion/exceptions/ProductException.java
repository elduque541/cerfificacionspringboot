package com.ias.certificacion.exceptions;

public abstract class ProductException extends RuntimeException {
    public ProductException(String message) {
        super(message);
    }
}
