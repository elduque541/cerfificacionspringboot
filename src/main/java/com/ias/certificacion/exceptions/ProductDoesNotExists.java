package com.ias.certificacion.exceptions;

public class ProductDoesNotExists extends ProductException {

    public ProductDoesNotExists() {
        super("Product Does Not Exists");
    }
}
