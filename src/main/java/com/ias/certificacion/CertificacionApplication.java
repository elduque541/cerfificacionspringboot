package com.ias.certificacion;

import com.ias.certificacion.exceptions.ProductException;
import com.ias.certificacion.domain.Product;
import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;

@Log
@SpringBootApplication
public class CertificacionApplication {

    public static void main(String[] args) {
        SpringApplication.run(CertificacionApplication.class, args);
    }

}