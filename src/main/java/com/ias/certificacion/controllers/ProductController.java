package com.ias.certificacion.controllers;

import com.ias.certificacion.services.ProductService;
import com.ias.certificacion.exceptions.ProductException;
import com.ias.certificacion.domain.Product;
import com.ias.certificacion.domain.ProductOperationRequest;
import com.ias.certificacion.operation.ProductOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        return awnser(productService.findById(id));
    }

    @GetMapping()
    public List<Product> findAll() {
        return productService.findAll().stream().map(ProductOperation::value).collect(Collectors.toList());
    }

    @PostMapping()
    public ResponseEntity insertOne(@RequestBody ProductOperationRequest productOperationRequest) {
        return awnser(productService.insertOne(productOperationRequest));
    }

    @PutMapping("/{id}")
    public ResponseEntity updateOne(@PathVariable Long id, @RequestBody ProductOperationRequest productOperationRequest) {
        return awnser(productService.updateOne(id, productOperationRequest));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOne(@PathVariable Long id) {
        return awnser(productService.deleteOne(id));
    }

    private ResponseEntity awnser(ProductOperation productOperation) {
        ResponseEntity responseEntity;

        if (productOperation.isValid()) {
            responseEntity = new ResponseEntity(productOperation.value(), HttpStatus.OK);
        } else {
            HttpStatus badRequest = productOperation.failure() instanceof ProductException ?
                    HttpStatus.UNAUTHORIZED : HttpStatus.INTERNAL_SERVER_ERROR;

            responseEntity = new ResponseEntity(productOperation.failure().getCause(), badRequest);
        }
        return responseEntity;
    }

}
