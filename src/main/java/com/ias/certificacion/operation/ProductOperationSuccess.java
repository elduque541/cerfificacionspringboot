package com.ias.certificacion.operation;

import com.ias.certificacion.domain.Product;

public class ProductOperationSuccess implements ProductOperation {

    private final Product product;

    public static ProductOperationSuccess of(Product product) {
        return new ProductOperationSuccess(product);
    }

    private ProductOperationSuccess(Product product) {
        this.product = product;
    }

    @Override
    public Product value() {
        return product;
    }

    @Override
    public Exception failure() {
        return null;
    }

    @Override
    public boolean isValid() {
        return true;
    }
}
