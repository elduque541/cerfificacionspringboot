package com.ias.certificacion.operation;

import com.ias.certificacion.domain.Product;

public class ProductOperationFailure implements ProductOperation {

    Exception exception;

    public static ProductOperationFailure of(Exception e) {
        return new ProductOperationFailure(e);
    }

    private ProductOperationFailure(Exception e) {
        this.exception = e;
    }

    @Override
    public Product value() {
        return null;
    }

    @Override
    public Exception failure() {
        return exception;
    }

    @Override
    public boolean isValid() {
        return false;
    }


}
