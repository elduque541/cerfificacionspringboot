package com.ias.certificacion.operation;

import com.ias.certificacion.domain.Product;

public interface ProductOperation {
    Product value();

    Exception failure();

    boolean isValid();
}
